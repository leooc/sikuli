package br.com.treinamento.sikuli;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;

public class YoutubeTest {
	
	private static WebDriver webDriver;
	private static final String URL_BEST_VIDEO_EVER = "https://www.youtube.com/watch?v=lAluSbUNkBo";

	public static void main(String[] args) throws FindFailed, InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\dev\\chromedriver_novo.exe");		
		webDriver = new ChromeDriver();
		webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		//Abre site da Cielo
		webDriver.get(URL_BEST_VIDEO_EVER);
		
		Screen s = new Screen();
		s.find("C:\\dev\\pause.png"); // identify pause button
		s.click("C:\\dev\\pause.png"); // click pause button
		System.out.println("Test pausou o vídeo no youtube.");
		s.find("C:\\dev\\play.png"); // identify play button
		s.click("C:\\dev\\play.png"); // click play button
		System.out.println("Test deu play no vídeo do youtube.");
	}

}
